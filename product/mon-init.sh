#!/bin/bash

# Configure hosts file
sudo echo '
${APP01_ADDRESS} app01
${APP02_ADDRESS} app02
${NFS_ADDRESS} nfs01
${DB01_ADDRESS} db01
localhost mon01
' >> /etc/hosts

# ---------------------------------------------- #
#               Install Prometheus               #
# ---------------------------------------------- #

# Install dependencies
sudo apt-get update
sudo apt-get install -y wget gnupg2

# Make prometheus user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Prometheus Monitoring User" prometheus

# Make directories and dummy files necessary for prometheus
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus
sudo touch /etc/prometheus/prometheus.yml
sudo touch /etc/prometheus/prometheus.rules.yml

# Assign ownership of the files above to prometheus user
sudo chown -R prometheus:prometheus /etc/prometheus
sudo chown prometheus:prometheus /var/lib/prometheus

# Download Prometheus
wget https://github.com/prometheus/prometheus/releases/download/v${PROM_VERSION}/prometheus-${PROM_VERSION}.linux-amd64.tar.gz
tar xvzf prometheus-${PROM_VERSION}.linux-amd64.tar.gz

# Copy utilities to filesystem directories
sudo cp prometheus-${PROM_VERSION}.linux-amd64/prometheus /usr/local/bin/
sudo cp prometheus-${PROM_VERSION}.linux-amd64/promtool /usr/local/bin/
sudo cp -r prometheus-${PROM_VERSION}.linux-amd64/consoles /etc/prometheus
sudo cp -r prometheus-${PROM_VERSION}.linux-amd64/console_libraries /etc/prometheus

# Assign the ownership of the tools above to prometheus user
sudo chown -R prometheus:prometheus /etc/prometheus/consoles
sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool

# Populate configuration files
sudo echo '
global:
  scrape_interval: 15s

rule_files:
  - 'prometheus.rules.yml'

scrape_configs:

  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['mon01:9100']
      - targets: ['app01:9100']
      - targets: ['app02:9100']
      - targets: ['nfs01:9100']
      - targets: ['db01:9100']

  - job_name: 'nginx exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['app01:9113']
      - targets: ['app02:9113']

  - job_name: 'mysqld_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['db01:9104']

  - job_name: 'grafana'
    scrape_interval: 5s
    static_configs:
      - targets:
        - localhost:3000

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - localhost:9093
' > /etc/prometheus/prometheus.yml

sudo echo '
groups:
  - name: vm_unavailable
    rules:
      - alert: InstanceDown
        expr: up == 0
        for: 1m
        labels:
          severity: page
        annotations:
          summary: "Instance {{ $labels.instance }} down"
          description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 1 minute."
' > /etc/prometheus/prometheus.rules.yml

sudo echo '
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/prometheus.service

# systemd
sudo systemctl daemon-reload
sudo systemctl enable prometheus
sudo systemctl start prometheus

# Installation cleanup
rm prometheus-${PROM_VERSION}.linux-amd64.tar.gz
rm -rf prometheus-${PROM_VERSION}.linux-amd64

# ---------------------------------------------- #
#               Install Grafana                  #
# ---------------------------------------------- #

# Install dependecies
sudo apt-get install -y adduser libfontconfig1

# Download Grafana
wget https://mirrors.huaweicloud.com/grafana/${GRAF_VERSION}/grafana-enterprise_${GRAF_VERSION}_amd64.deb

# Install grafana
sudo dpkg -i grafana-enterprise_${GRAF_VERSION}_amd64.deb

# Configure Grafana to listen to any interface
sed -i 's/;http_addr =/http_addr =/g'  /etc/grafana/grafana.ini

# Configure Grafana datasource
sudo echo '
# Configuration file version
apiVersion: 1

datasources:
  - name: Prometheus-GB
    type: prometheus
    access: proxy
    orgId: 1
    url: http://localhost:9090
    basicAuth: false
    isDefault: false
    jsonData:
      timeInterval: "5s"
      httpMethod: "GET"
      tlsSkipVerify: false
      version: 1
' > /etc/grafana/provisioning/datasources/prometheus-gb.yml

# Download Grafana.com dashboard ID 1860 (Node-Exporter-Full)
sudo mkdir -p /var/lib/grafana/dashboards
wget -O /var/lib/grafana/dashboards/node-exporter.json https://grafana.com/api/dashboards/1860/revisions/30/download

# Download Grafana.com dashboard ID 12708 (Nginx-Exporter)
wget -O /var/lib/grafana/dashboards/nginx-exporter.json https://grafana.com/api/dashboards/12708/revisions/1/download

# Download Grafana.com dashboard ID 14057 (MySQL-Exporter)
wget -O /var/lib/grafana/dashboards/mysql-exporter.json https://grafana.com/api/dashboards/14057/revisions/1/download

# Configure Grafana dashboard
sudo echo '
apiVersion: 1
providers:
- name: 'node-exporter'
  orgId: 1
  folder: ''
  type: file
  disableDeletion: false
  updateIntervalSeconds: 10
  options:
    path: /var/lib/grafana/dashboards
    foldersFromFilesStructure: true
' > /etc/grafana/provisioning/dashboards/node-exporter.yml

# systemd
sudo systemctl daemon-reload
sudo systemctl enable grafana-server
sudo systemctl start grafana-server

# Installation cleanup
rm grafana-enterprise_${GRAF_VERSION}_amd64.deb


# ---------------------------------------------- #
#             Install Alertmanager               #
# ---------------------------------------------- #

# Make alertmanager user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Alertmanager User" alertmanager

# Make directories and dummy files necessary for alertmanager
sudo mkdir /etc/alertmanager
sudo mkdir /etc/alertmanager/template
sudo mkdir -p /var/lib/alertmanager/data
sudo touch /etc/alertmanager/alertmanager.yml


sudo chown -R alertmanager:alertmanager /etc/alertmanager
sudo chown -R alertmanager:alertmanager /var/lib/alertmanager

# Download alertmanager and copy utilities to where they should be in the filesystem
wget https://github.com/prometheus/alertmanager/releases/download/v${ALERT_VERSION}/alertmanager-${ALERT_VERSION}.linux-amd64.tar.gz
tar xvzf alertmanager-${ALERT_VERSION}.linux-amd64.tar.gz

sudo cp alertmanager-${ALERT_VERSION}.linux-amd64/alertmanager /usr/local/bin/
sudo cp alertmanager-${ALERT_VERSION}.linux-amd64/amtool /usr/local/bin/
sudo chown alertmanager:alertmanager /usr/local/bin/alertmanager
sudo chown alertmanager:alertmanager /usr/local/bin/amtool

# Populate configuration files
sudo echo '
global:

templates:
- '/etc/alertmanager/template/*.tmpl'

route:
  repeat_interval: 3h
  receiver: gb-telegram

receivers:
- name: 'gb-telegram'
  telegram_configs:
  - bot_token: ${BOT_TOKEN}
    api_url: https://api.telegram.org
    chat_id: ${BOT_CHAT}
    parse_mode: 'HTML'
' > /etc/alertmanager/alertmanager.yml

sudo echo '
[Unit]
Description=Prometheus Alert Manager service
Wants=network-online.target
After=network.target

[Service]
User=alertmanager
Group=alertmanager
Type=simple
ExecStart=/usr/local/bin/alertmanager \
    --config.file /etc/alertmanager/alertmanager.yml \
    --storage.path /var/lib/alertmanager/data
Restart=always

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/alertmanager.service

# systemd
sudo systemctl daemon-reload
sudo systemctl enable alertmanager
sudo systemctl start alertmanager

# Installation cleanup
rm alertmanager-${ALERT_VERSION}.linux-amd64.tar.gz
rm -rf alertmanager-${ALERT_VERSION}.linux-amd64


# ---------------------------------------------- #
#             Install NodeExporter               #
# ---------------------------------------------- #

# Make node_exporter user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter

# Download node_exporter and copy utilities to where they should be in the filesystem
wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_VERSION}/node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
tar xvzf node_exporter-${NODE_VERSION}.linux-amd64.tar.gz

sudo cp node_exporter-${NODE_VERSION}.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter

# systemd
sudo echo '
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter

# Installation cleanup
rm node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
rm -rf node_exporter-${NODE_VERSION}.linux-amd64