# ---------------------------------------------- #
#               Deploy Monitoring VM             #
# ---------------------------------------------- #

# Create MON instance MON01
resource "vkcs_compute_instance" "mon01" {
  name      = var.mon01_name
  flavor_id = data.vkcs_compute_flavor.mon.id
  security_groups = [
    vkcs_networking_secgroup.mon.name
  ]
  image_name        = data.vkcs_images_image.ubuntu.name
  availability_zone = "GZ1"
  key_pair          = var.keypair_name
  config_drive      = true
  user_data = templatefile("../product/mon-init.sh", {
    APP01_ADDRESS = vkcs_compute_instance.app01.access_ip_v4,
    APP02_ADDRESS = vkcs_compute_instance.app02.access_ip_v4,
    DB01_ADDRESS  = vkcs_db_instance.db01.ip[0],
    NFS_ADDRESS   = vkcs_compute_instance.nfs01.access_ip_v4,
    GRAF_VERSION  = var.grafana_version,
    PROM_VERSION  = var.prometheus_version,
    NODE_VERSION  = var.node_exporter_version,
    ALERT_VERSION = var.alertmanager_version,
    NGINX_VERSION = var.nginx_exporter_version,
    BOT_CHAT      = var.bot_chat,
    BOT_TOKEN     = var.bot_token
  })

  block_device {
    uuid                  = data.vkcs_images_image.ubuntu.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = var.mon_volume_type
    volume_size           = var.mon_volume_size
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid        = vkcs_networking_network.project_network.id
    fixed_ip_v4 = "10.0.1.110"
  }
  depends_on = [
    vkcs_networking_router_interface.router_interface
  ]
}

resource "vkcs_networking_floatingip" "mon_fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "mon_fip" {
  floating_ip = vkcs_networking_floatingip.mon_fip.address
  instance_id = vkcs_compute_instance.mon01.id
}
