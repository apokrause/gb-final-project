# ---------------------------------------------- #
#             Deploy Project network             #
# ---------------------------------------------- #

# Search for external network IPs
data "vkcs_networking_network" "extnet" {
  name = "ext-net"
}

# Create project network
resource "vkcs_networking_network" "project_network" {
  name           = var.network_name
  admin_state_up = "true"
  description    = "Project network"
}

# Create project subnet
resource "vkcs_networking_subnet" "project_subnet" {
  name       = var.subnet_name
  network_id = vkcs_networking_network.project_network.id
  cidr       = var.subnet_cidr
  allocation_pool {
    end   = "10.0.1.99"
    start = "10.0.1.50"
  }
}

# Create project router
resource "vkcs_networking_router" "router" {
  name                = "project_router"
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.extnet.id
}

# Add SNAT and INTERFACE_DISTRIBUTED to project_subnet
resource "vkcs_networking_router_interface" "router_interface" {
  router_id = vkcs_networking_router.router.id
  subnet_id = vkcs_networking_subnet.project_subnet.id
  depends_on = [
    vkcs_networking_router.router
  ]
}

# ---------------------------------------------- #
#             Deploy network security            #
# ---------------------------------------------- #

# APP servers Security groups
resource "vkcs_networking_secgroup" "app" {
  name        = "app_fw"
  description = "APP servers security group"
}

resource "vkcs_networking_secgroup_rule" "app_rule_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_http" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_https" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_node_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.app.id
}

resource "vkcs_networking_secgroup_rule" "app_rule_nginx_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9113
  port_range_max    = 9113
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.app.id
}

# DBaaS servers Security groups are managed by VK Cloud

# NFS servers Security groups
resource "vkcs_networking_secgroup" "nfs" {
  name        = "nfs_fw"
  description = "NFS servers security group"
}

resource "vkcs_networking_secgroup_rule" "nfs_rule_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.nfs.id
}

resource "vkcs_networking_secgroup_rule" "nfs_rule_rpcbind" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 111
  port_range_max    = 111
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.nfs.id
}

resource "vkcs_networking_secgroup_rule" "nfs_rule_nfs" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 2049
  port_range_max    = 2049
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.nfs.id
}

resource "vkcs_networking_secgroup_rule" "nfs_rule_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.nfs.id
}

resource "vkcs_networking_secgroup_rule" "nfs_rule_mountd" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 38467
  port_range_max    = 38467
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.nfs.id
}

# MON servers Security groups
resource "vkcs_networking_secgroup" "mon" {
  name        = "mon_fw"
  description = "MON servers security group"
}

resource "vkcs_networking_secgroup_rule" "mon_rule_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}

resource "vkcs_networking_secgroup_rule" "mon_rule_grafana" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 3000
  port_range_max    = 3000
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}

resource "vkcs_networking_secgroup_rule" "mon_rule_prometheus" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9090
  port_range_max    = 9090
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}

resource "vkcs_networking_secgroup_rule" "mon_rule_alertmanager" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9093
  port_range_max    = 9093
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.mon.id
}

resource "vkcs_networking_secgroup_rule" "mon_rule_node_exporter" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = var.subnet_cidr
  security_group_id = vkcs_networking_secgroup.mon.id
}
